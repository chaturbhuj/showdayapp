import { Component, OnInit } from '@angular/core';
import { api_url,property_image_path} from '../constant/constant';
import { CommonService } from '../common/common.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.page.html',
  styleUrls: ['./property-list.page.scss'],
})
export class PropertyListPage implements OnInit {
	property_data:any =[];
	property_image_path:any='';
	constructor(
		private api: CommonService,
		private router: Router,
		private activatedRoute: ActivatedRoute, 
	) { }
 
	ngOnInit() {
		this.property_image_path = property_image_path();
		this.get_property();
	}	
	
	get_property(){
		const comman_data = {
			myurl: api_url()+'products/index'
		};
		this.api.comman_funnction_call(comman_data).then((res)=>{
			console.log(res);
			this.property_data = res;
			
		});
	}
	
	property_detail(id){
		this.router.navigate([id ? `/property-detail/${id}` : `/property-detail`], { state: { id: id}});
	}
}
