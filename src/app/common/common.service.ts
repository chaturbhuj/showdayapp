import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class CommonService {

	constructor(
		private http: HttpClient, 
		private httpClient: HttpClient,
	) { }
  
	async comman_funnction_call(comman_data) {
		console.log(comman_data);
		return new Promise<any>((resolve, reject) => {

			this.http.post(comman_data.myurl, comman_data).subscribe((res) => {
				resolve(res)
			});
		});
	}
	
	async comman_funnction_call_get(comman_data) {
		console.log(comman_data);
		return new Promise<any>((resolve, reject) => {

			this.http.get(comman_data.myurl).subscribe((res) => {
				resolve(res)
			});
		});
	}

	/*async comman_funnction_call_with_auth_post(comman_data) {
		this.sellertoken = JSON.parse(localStorage.getItem('f_r_flatesellertoken'));
		let header = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + this.sellertoken)
		console.log(comman_data);
		return new Promise<any>((resolve, reject) => {

			this.http.post(comman_data.myurl, comman_data, { headers: header }).subscribe((res) => {
				resolve(res)
			});
		});
	}*/
  
  
}
