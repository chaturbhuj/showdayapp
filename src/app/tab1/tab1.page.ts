import { Component } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
	selector: 'app-tab1',
	templateUrl: 'tab1.page.html',
	styleUrls: [ 'tab1.page.scss' ]
})
export class Tab1Page {
	films: Array<any>;
	httpOptions = {
		headers: new HttpHeaders({ content_id: '1' })
	};
	constructor(public httpClient: HttpClient) {
		this.httpClient
			.post('https://owlblink.com/index.php/api/publicpages/fatch_public_content_data', { content_id: '1' })
			.subscribe((data) => {
				console.log('my data: ', data);
			});
	}
}
