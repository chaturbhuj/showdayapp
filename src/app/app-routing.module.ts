import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'property-list',
    loadChildren: () => import('./property-list/property-list.module').then( m => m.PropertyListPageModule)
  },
  {
    path: 'property-detail/:id',
    loadChildren: () => import('./property-detail/property-detail.module').then( m => m.PropertyDetailPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
