import { Component, OnInit } from '@angular/core';
import { api_url,property_image_path} from '../constant/constant';
import { CommonService } from '../common/common.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-property-detail',
  templateUrl: './property-detail.page.html',
  styleUrls: ['./property-detail.page.scss'],
})
export class PropertyDetailPage implements OnInit {
	property_id:any = 0;
	property_data:any = [];
	property_images:any = [];
	property_image_path:any='';
	slideOpts = {
		initialSlide: 1,
		speed: 400
	};
	constructor(
		private api: CommonService,
		private router: Router,
		private activatedRoute: ActivatedRoute, 
	) { 
		if(this.router.getCurrentNavigation()?.extras?.state){
		   this.property_id = this.router.getCurrentNavigation().extras?.state?.id; 
		} else {
			this.router.navigate([`/property-list`]); 
		}
		console.log(this.property_id);
	}

	ngOnInit() {
	
	}
	
	ionViewWillEnter() {
		this.property_image_path = property_image_path();
		if(this.property_id != 0){
			this.get_property_by_id();
		}
	}
	
	get_property_by_id(){
		const comman_data = {
			myurl: api_url()+'products/show/'+this.property_id
		};
		this.api.comman_funnction_call(comman_data).then((res)=>{
			this.property_data = res[0];
			if(res[0].image_path){
				this.property_images = JSON.parse(res[0].image_path);
			}
		});
	}
	
}
